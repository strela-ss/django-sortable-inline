from django import forms
from django.utils.safestring import mark_safe
from mywidget.models import Image
from django.template import loader, Context, Template

__author__ = 'Vladislav Strelnikov'


def find_in_arr(arr, id):
    for i in arr:
        if i[0] == id:
            return i


class MyWidget(forms.TextInput):

    def render(self, name, value, attrs=None):
        if value and value != u'None':
            arr = value.split(",")
            imgs = Image.objects.filter(id__in=arr).values('id', 'img')
            imgs_li = "".join(["<li class='ui-state-default'><img class='fixed-image' src='%s'></li>"
                               % ("/media/%s" % find_in_arr(imgs, i)['img']) for i in arr])
        else:
            value = ""
            imgs = Image.objects.all().values('id', 'img')
            imgs_li = "".join(["<li class='ui-state-default'><img class='fixed-image' src='%s'></li>"
                               % ("/media/%s" % i['img']) for i in imgs])
        template = loader.get_template('mywidget/sorting.html')
        context = Context({
            'name': name,
            'value': value,
            'list': mark_safe(imgs_li),
        })
        html = template.render(context)
        return mark_safe(html)

    class Media(object):
        js = ("https://code.jquery.com/jquery-1.12.4.js",
              "https://code.jquery.com/ui/1.12.1/jquery-ui.js",
              "js/mywidget.js",)
        css = {
            'all': (
                '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
            )
        }
