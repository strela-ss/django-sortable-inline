from django.db import models

# Create your models here.


class Galery(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)

    def __unicode__(self):
        return self.name


class Image(models.Model):
    img = models.ImageField(upload_to='img')
    galery = models.ForeignKey(Galery, null=False, blank=False)
    ordering = models.PositiveSmallIntegerField(null=False, blank=False)
