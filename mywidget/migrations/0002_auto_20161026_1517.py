# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mywidget', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='galery',
            name='sorting',
        ),
        migrations.AddField(
            model_name='galery',
            name='name',
            field=models.CharField(default=1, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='image',
            name='ordering',
            field=models.PositiveSmallIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='image',
            name='img',
            field=models.ImageField(upload_to=b'img'),
        ),
    ]
