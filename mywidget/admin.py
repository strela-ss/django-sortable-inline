from django.contrib import admin
from django.contrib.admin.options import InlineModelAdmin
from mywidget.forms import GaleryForm, ImageForm
from mywidget.models import Galery, Image


class MyDefaultInline(InlineModelAdmin):
    template = 'mywidget/defaultinline.html'

    class Media:
        pass


class Imageinline(MyDefaultInline):
    model = Image
    form = ImageForm
    fields = ('id', 'img', 'ordering')
    readonly_fields = ('id',)
    extra = 0

    def get_queryset(self, request):
        return super(Imageinline, self).get_queryset(request).order_by('ordering')


class GaleryAdmin(admin.ModelAdmin):
    form = GaleryForm
    inlines = [Imageinline]


class ImageAdmin(admin.ModelAdmin):
    fields = ('id', 'img')


admin.site.register(Image, ImageAdmin)
admin.site.register(Galery, GaleryAdmin)
