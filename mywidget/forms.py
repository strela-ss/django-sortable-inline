from django import forms
from mywidget.models import Galery, Image
from mywidget.widgets import MyWidget

__author__ = 'Vladislav Strelnikov'


class ImageForm(forms.ModelForm):
    ordering = forms.IntegerField(widget=forms.HiddenInput)

    class Meta:
        model = Image
        fields = '__all__'


class GaleryForm(forms.ModelForm):
    class Meta:
        model = Galery
        fields = '__all__'
